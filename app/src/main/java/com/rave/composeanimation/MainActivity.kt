package com.rave.composeanimation

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.*
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.rave.composeanimation.components.Greeting
import com.rave.composeanimation.components.Instagram
import com.rave.composeanimation.ui.theme.ComposeAnimationTheme


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeAnimationTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    var triggerAnimation by remember {
                        mutableStateOf(false)
                    }
                    var expanded by remember {
                        mutableStateOf(false)
                    }

                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                    ) {
                        Button(
                            onClick = {
                                expanded = !expanded
                                triggerAnimation = !triggerAnimation
                            },
                            colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent)
                        ) {
                            Greeting("RaveBizz")
                        }

                        DropdownMenu(
                            expanded = expanded, onDismissRequest = {
                                expanded = false
                            },
                            modifier = Modifier.padding(70.dp)
                        ) {
                            AnimatedVisibility(
                                visible = triggerAnimation,
                                enter = slideInHorizontally(animationSpec = tween(durationMillis = 500)) { fullWidth ->
                                    // Offsets the content by 1/3 of its width to the left, and slide towards right
                                    // Overwrites the default animation with tween for this slide animation.
                                    -fullWidth / 3
                                } + fadeIn(
                                    // Overwrites the default animation with tween
                                    animationSpec = tween(durationMillis = 200)
                                ),
                                exit = slideOutHorizontally(animationSpec = spring(stiffness = Spring.StiffnessHigh)) {
                                    // Overwrites the ending position of the slide-out to 200 (pixels) to the right
                                    500
                                } + fadeOut()
                            )
                            {

                                IconButton(
                                    onClick = {
                                        Toast.makeText(
                                            applicationContext,
                                            "INSTA",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    },
                                ) {
                                    Instagram(size = 100, triggerAnimation)
                                }
                            }
                        }
                    }
                }

            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeAnimationTheme {
        Greeting("Android")
    }
}
