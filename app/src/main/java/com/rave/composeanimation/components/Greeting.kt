/**
 * Created by Jimmy McBride on 2022-12-05
 *
 * Copyright © 2022 Jimmy McBride
 */
package com.rave.composeanimation.components

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.font.FontFamily

import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.unit.sp

/**
 * Greeting
 *
 * @author Jimmy McBride on 2022-12-05.
 */
@Composable
fun Greeting(name: String) {
  Text(
    text = "Hello, $name!",
    fontSize = 26.sp,
    fontWeight = Bold,
    fontFamily = FontFamily.Monospace
  )
}
