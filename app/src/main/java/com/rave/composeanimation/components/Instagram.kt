/**
 * Created by Jimmy McBride on 2022-12-05
 *
 * Copyright © 2022 Jimmy McBride
 */
package com.rave.composeanimation.components

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.*

/**
 * Instagram
 *
 * @author Jimmy McBride on 2022-12-05.
 */
@Composable
fun Instagram(
    size: Int,
    triggerAnimation: Boolean,
) {

    var state by remember { mutableStateOf(ColorState.FIRST) }
    val colorTweenDuration = 1000
    val colorTweenEasing = LinearEasing

    LaunchedEffect(key1 = triggerAnimation) {
        while (triggerAnimation) {
            delay(colorTweenDuration.toLong())
            state = when (state) {
                ColorState.FIRST -> ColorState.SECOND
                ColorState.SECOND -> ColorState.THIRD
                ColorState.THIRD -> ColorState.FIRST
            }
        }
    }

    val firstColor = animateColorAsState(
        targetValue = when (state) {
            ColorState.FIRST -> Color.Yellow
            ColorState.SECOND -> Color.Magenta
            ColorState.THIRD -> Color.Red
        }, animationSpec = tween(colorTweenDuration, easing = colorTweenEasing)
    )

    val secondColor = animateColorAsState(
        targetValue = when (state) {
            ColorState.FIRST -> Color.Red
            ColorState.SECOND -> Color.Yellow
            ColorState.THIRD -> Color.Magenta
        }, animationSpec = tween(colorTweenDuration, easing = colorTweenEasing)
    )

    val thirdColor = animateColorAsState(
        targetValue = when (state) {
            ColorState.FIRST -> Color.Magenta
            ColorState.SECOND -> Color.Red
            ColorState.THIRD -> Color.Yellow
        }, animationSpec = tween(colorTweenDuration, easing = colorTweenEasing)
    )

    val instagramColors = listOf(
        firstColor.value, secondColor.value, thirdColor.value
    )
    Canvas(modifier = Modifier
        .size(size.dp)
        .padding(16.dp),
        onDraw = {
            drawRoundRect(
                brush = Brush.linearGradient(colors = instagramColors),
                cornerRadius = CornerRadius(60f, 60f),
                style = Stroke(width = 15f, cap = StrokeCap.Round)
            )
            drawCircle(
                brush = Brush.linearGradient(colors = instagramColors),
                radius = 45f,
                style = Stroke(width = 15f, cap = StrokeCap.Round)
            )
            drawCircle(
                brush = Brush.linearGradient(colors = instagramColors),
                radius = 13f,
                center = Offset(this.size.width * .80f, this.size.height * 0.20f),
            )
        }
    )
}

enum class ColorState {
    FIRST, SECOND, THIRD;
}
